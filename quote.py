# -*- coding: future_fstrings -*-

#    Quotes module for Friendly-Telegram
#    Copyright (C) 2019 Pavel (pqhaz)

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import logging
import requests

from .. import loader, utils
from PIL import Image
from urllib.request import urlretrieve
from telethon.tl.functions.channels import GetParticipantRequest
from telethon.tl.functions.users import GetFullUserRequest
from telethon.tl.types import (MessageEntityBold, MessageEntityItalic,
                               MessageEntityMention, MessageEntityTextUrl,
                               MessageEntityCode, MessageEntityMentionName,
                               MessageEntityHashtag, MessageEntityCashtag,
                               MessageEntityBotCommand, Channel)

logger = logging.getLogger(__name__)


def register(cb):
    cb(QuotesMod())


class QuotesMod(loader.Module):
    """Description for module"""
    def __init__(self):
        self.config = loader.ModuleConfig("API_TOKEN", _('xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxx'),
                                          "API Token for quotes.")
        self.name = _("Quotes")

    async def client_ready(self, client, db):
        self.client = client

    async def quotecmd(self, message):  # noqa: C901
        """Quote a message.
        Usage: .quote (optional: file/force_file/sticker)
        Sends as sticker by default
        Needs an API Token."""
        args = utils.get_args(message)
        reply = await message.get_reply_message()

        if not reply:
            return await utils.answer(message, '<code>You didn\'t reply to a message.</code>')

        if reply.media:
            msg = await message.edit('<code>Processing... Note: only message will get processed.</code>')
        else:
            msg = await message.edit('<code>Processing...</code>')

        username = ''
        no_pfp = 'True'
        colour = '#b48bf2'
        admintitle = ''
        username = ''
        pfp = ''
        user_id = 000000000
        fwd_status = 'no_fwd'
        if reply.fwd_from:
            fwd = reply.fwd_from
            if fwd.from_name:  # hidden
                username = fwd.from_name
                no_pfp = True
                colour = '#b48bf2'
                admintitle = ''
                pfp = ''
                fwd_status = 'hidden'
            else:
                fwd_status = 'open'

        if fwd_status != 'hidden':
            if fwd_status == 'open':
                user_id = reply.fwd_from.from_id
            elif fwd_status == 'no_fwd':
                user_id = reply.from_id

            if not await self.client.get_profile_photos(user_id):
                no_pfp = True
            else:
                no_pfp = False

            dl_pfp = await self.client.download_profile_photo(user_id)
            if no_pfp is False:
                pfp = upload_to_0x0st(dl_pfp)
            else:
                pfp = ''
            colors = ["#fb6169", "#85de85", "#f3bc5c", "#65bdf3", "#b48bf2", "#ff5694", "#62d4e3", "#faa357"]
            num1 = user_id % 7
            num2 = [0, 7, 4, 1, 6, 3, 5]
            colour = colors[num2[num1]]
            if fwd_status == 'no_fwd' and isinstance(message.chat, Channel):
                participant = await self.client(GetParticipantRequest(message.chat_id, user_id))
                user = participant.users[0]
            else:
                user = (await self.client(GetFullUserRequest(user_id))).user
            username = user.first_name
            if user.last_name:
                username += f' {user.last_name}'
            try:
                admintitle = participant.participant.rank
                if not admintitle:
                    admintitle = 'admin'
            except (AttributeError, UnboundLocalError):
                admintitle = ''

        token = self.config["API_TOKEN"]

        requested = requests.post('http://v158828.hosted-by-vdsina.ru/quote', data={
            'pfp': pfp.encode('utf-8'),
            'no_pfp': str(no_pfp).encode('utf-8'),
            'colour': colour.encode('utf-8'),
            'username': username.encode('utf-8'),
            'admintitle': admintitle.encode('utf-8'),
            'raw_text': process_entities(reply).encode('utf-8'),
            'token': token.encode('utf-8'),
        })
        if requested.status_code == 500:
            return await utils.answer(message, 'bruh moment, api error')
        else:
            requested = requested.json()

        try:
            urlretrieve(requested['success']['download'], 'file.png')
            await msg.delete()
            if not args or args[0].lower() == 'sticker':
                Image.open('file.png').save('file.webp', 'webp')
                await self.client.send_file(message.chat_id, 'file.webp', reply_to=reply.id)
                os.remove('file.webp')
            else:
                test = args[0].lower()
                if test == 'file':
                    await self.client.send_file(message.chat_id, 'file.png', reply_to=reply.id)
                    os.remove('file.png')
                elif test == 'force_file':
                    await self.client.send_file(message.chat_id, 'file.png', reply_to=reply.id,
                                                force_document=True)
                    os.remove('file.png')
        except KeyError:
            if requested['401']:
                if requested['401'] == 'wrong token.':
                    return await utils.answer(message, '<code>Wrong API Token.</code>')


def process_entities(reply):
    if not reply.entities:
        return reply.raw_text

    raw_text = reply.raw_text
    text = [x for x in raw_text]
    for entity in reply.entities:
        entity_type = type(entity)
        start = entity.offset
        end = start + (entity.length - 1)
        if entity_type is MessageEntityBold:
            text[start] = '{b}' + text[start]
            text[end] = text[end] + '{cl}'
        elif entity_type is MessageEntityItalic:
            text[start] = '{i}' + text[start]
            text[end] = text[end] + '{cl}'
        elif entity_type in [MessageEntityMention, MessageEntityTextUrl,
                             MessageEntityMentionName, MessageEntityHashtag,
                             MessageEntityCashtag, MessageEntityBotCommand]:
            text[start] = '{l}' + text[start]
            text[end] = text[end] + '{cl}'
        elif entity_type is MessageEntityCode:
            text[start] = '{c}' + text[start]
            text[end] = text[end] + '{cl}'

    return ''.join(text)


def upload_to_0x0st(path):
    req = requests.post('https://0x0.st', files={'file': open(path, 'rb')})
    os.remove(path)
    return req.text
